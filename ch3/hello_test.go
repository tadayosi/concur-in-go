package ch3

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func sayHello1() {
	fmt.Println("hello")
}

func TestHello(t *testing.T) {
	go sayHello1()
	go func() {
		fmt.Println("hello")
	}()
	sayHello2 := func() {
		fmt.Println("hello")
	}
	go sayHello2()

	time.Sleep(100 * time.Millisecond)
}

func TestHelloWait(t *testing.T) {
	var wg sync.WaitGroup
	sayHello := func() {
		defer wg.Done()
		fmt.Println("hello")
	}
	wg.Add(1)
	go sayHello()
	wg.Wait()
}

func TestHelloClosure1(t *testing.T) {
	var wg sync.WaitGroup
	salutation := "hello"
	wg.Add(1)
	go func() {
		defer wg.Done()
		salutation = "welcome"
	}()
	wg.Wait()
	fmt.Println(salutation)
}

func TestHelloClosure2(t *testing.T) {
	var wg sync.WaitGroup
	for _, salutation := range []string{"hello", "greetings", "good day"} {
		wg.Add(1)
		go func(salutation string) {
			defer wg.Done()
			fmt.Println(salutation)
		}(salutation)
	}
	wg.Wait()
}
