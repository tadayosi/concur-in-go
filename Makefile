test: test-ch3 test-ch4

clean-cache:
	go clean -testcache

test-ch3: clean-cache
	go test -v ./ch3/...

test-ch4: clean-cache
	go test -v -bench . ./ch4/... -benchmem

lint:
	golangci-lint run
