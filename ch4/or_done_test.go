package ch4

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestOrDone1(t *testing.T) {
	newRandStream := func(done <-chan interface{}) <-chan interface{} {
		randStream := make(chan interface{})
		go func() {
			defer fmt.Println("newRandStream closure exited.")
			defer close(randStream)
			for {
				select {
				case randStream <- rand.Int():
					time.Sleep(200 * time.Millisecond)
				case <-done:
					return
				}
			}
		}()
		return randStream
	}

	done := make(chan interface{})
	go func() {
		time.Sleep(1 * time.Second)
		close(done)
	}()

	myChan := newRandStream(done)
	for val := range orDone(done, myChan) {
		fmt.Printf("%d\n", val)
	}
}
